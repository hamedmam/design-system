module.exports = {
  arrowParens: 'avoid',
  semi: false,
  bracketSpacing: true,
  printWidth: 120,
  singleQuote: true,
  tabWidth: 2
}

const Button = require('./packages/Button/src/Button').default
const Grid = require('./packages/Grid/src/Grid').default
const Col = require('./packages/Grid/src/Col/Col.jsx').default
const Row = require('./packages/Grid/src/Row/Row').default

global.Button = Button
global.Grid = Grid
global.Row = Row
global.Col = Col

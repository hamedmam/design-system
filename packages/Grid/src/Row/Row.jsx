import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'styled-components'
import { Grid, Row as GridRow } from 'react-styled-flexboxgrid'

const DIMENSION_NAMES = ['xs', 'sm', 'md', 'lg']

const getTheme = gutterWidth => ({
  flexboxgrid: {
    // Defaults
    gridSize: 12, // columns
    gutterWidth, // rem
    outerMargin: 0, // rem
    mediaQuery: 'only screen',
    container: {
      sm: 46, // rem
      md: 61, // rem
      lg: 76 // rem
    },
    breakpoints: {
      xs: 0, // em
      sm: 48, // em
      md: 64, // em
      lg: 75 // em
    }
  }
})

function Row({
  children,
  hasLimitedWidth,
  isReversed,
  gutterWidth,
  start,
  center,
  end,
  top,
  middle,
  bottom,
  around,
  between,
  first,
  last
}) {
  return (
    <ThemeProvider theme={getTheme(gutterWidth)}>
      <Grid fluid={!hasLimitedWidth}>
        <GridRow
          reverse={isReversed}
          start={start}
          center={center}
          end={end}
          top={top}
          middle={middle}
          bottom={bottom}
          around={around}
          between={between}
          first={first}
          last={last}
        >
          {children}
        </GridRow>
      </Grid>
    </ThemeProvider>
  )
}

Row.propTypes = {
  hasLimitedWidth: PropTypes.bool,
  isReversed: PropTypes.bool,
  gutterWidth: PropTypes.number,
  start: PropTypes.oneOf(DIMENSION_NAMES),
  center: PropTypes.oneOf(DIMENSION_NAMES),
  end: PropTypes.oneOf(DIMENSION_NAMES),
  top: PropTypes.oneOf(DIMENSION_NAMES),
  middle: PropTypes.oneOf(DIMENSION_NAMES),
  bottom: PropTypes.oneOf(DIMENSION_NAMES),
  around: PropTypes.oneOf(DIMENSION_NAMES),
  between: PropTypes.oneOf(DIMENSION_NAMES),
  first: PropTypes.oneOf(DIMENSION_NAMES),
  last: PropTypes.oneOf(DIMENSION_NAMES),
  children: PropTypes.node.isRequired
}

Row.defaultProps = {
  hasLimitedWidth: true,
  isReversed: false,
  gutterWidth: 0,
  start: null,
  center: null,
  end: null,
  top: null,
  middle: null,
  bottom: null,
  around: null,
  between: null,
  first: null,
  last: null
}

export default Row

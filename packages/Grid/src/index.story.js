import React from 'react'
import { storiesOf } from '@storybook/react'

import Grid from '.'
const { Row, Col } = Grid

function getRandomColor() {
  var letters = '0123456789ABCDEF'
  var color = '#'
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

const PlaceHolder = () => <div style={{ background: getRandomColor(), width: '100%', height: 30 }} />

storiesOf('Grid', module).add('default', () => (
  <>
    <Row>
      <Col>
        <PlaceHolder />
      </Col>
      <Col>
        <PlaceHolder />
      </Col>
      <Col>
        <PlaceHolder />
      </Col>
    </Row>
    <Row>
      <Col>
        <PlaceHolder />
      </Col>
      <Col>
        <PlaceHolder />
      </Col>
    </Row>
  </>
))

import React from 'react'
import PropTypes from 'prop-types'
import { Col as Column } from 'react-styled-flexboxgrid'

function filterColumnProps(columnProps) {
  const propsWithValue = Object.keys(columnProps).filter(key => columnProps[key])
  const validProps = {}
  for (const property of propsWithValue) {
    validProps[property] = columnProps[property]
  }
  return validProps
}

export default function Col({ children, xs, sm, md, lg, xsOffset, smOffset, mdOffset, lgOffset }) {
  const columnOrginalProps = {
    xs,
    sm,
    md,
    lg,
    xsOffset,
    smOffset,
    mdOffset,
    lgOffset
  }
  const columnProps = filterColumnProps(columnOrginalProps)
  return <Column {...columnProps}>{children}</Column>
}

Col.propTypes = {
  /**
   * Specify number of columns within the 'xs' breakpoint range. `0` hides the column.
   *
   * `true` sets the column width automatically;
   * `false` disables the prop
   *
   * @since 1.2.0
   */
  xs: PropTypes.any,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xsOffset: PropTypes.number,
  smOffset: PropTypes.number,
  mdOffset: PropTypes.number,
  lgOffset: PropTypes.number,
  children: PropTypes.node.isRequired
}

Col.defaultProps = {
  xs: true,
  sm: null,
  md: null,
  lg: null,
  xsOffset: null,
  smOffset: null,
  mdOffset: null,
  lgOffset: null
}

import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'

import Grid from '.'
const { Row, Col } = Grid

describe('Grid', () => {
  test('renders correctly', () => {
    const tree = renderer
      .create(
        <Row>
          <Col>{'Test'}</Col>
        </Row>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})

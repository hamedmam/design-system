import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

function setBackgroundColor({ variant }) {
  if (variant === 'secondary') {
    return '#d67a38'
  }
  return '#13395d'
}
const StyledButton = styled.button(
  props => `
  background: ${setBackgroundColor(props)};
  color: #fff;
  border-radius: 20px;
  cursor: pointer;
  font-size: 1rem;
  font-weight: 300;
  padding: 9px 36px;
`
)

const ButtonTextWrapper = styled.span({
  width: '100%'
})

const Button = ({ type, variant, children }) => {
  return (
    <StyledButton variant={variant} type={type}>
      <ButtonTextWrapper>{children}</ButtonTextWrapper>
    </StyledButton>
  )
}

Button.propTypes = {
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  variant: PropTypes.oneOf(['primary', 'secondary', 'inverted']),
  children: PropTypes.string.isRequired
}

Button.defaultProps = {
  type: 'button',
  variant: 'primary'
}

export default Button

const path = require('path')
const { version } = require('./package')

module.exports = {
  title: 'Hamed Design System',
  skipComponentsWithoutExample: true,
  require: [path.resolve(__dirname, './styleguide.setup.js')],
  getExampleFilename(componentPath) {
    return componentPath.replace(/\.jsx?$/, '.md')
  },
  getComponentPathLine(componentPath) {
    const name = path.basename(componentPath, '.jsx')

    const kebabizeName = name
      .split(/(?=[A-Z])/)
      .join('-')
      .toLowerCase()
    if (name === 'Grid' || name === 'Row' || name === 'Col') {
      return `import { Row, Col } from '@hmds/grid'`
    }

    return `import ${name} from '@hmds/${kebabizeName}'`
  },
  moduleAliases: {
    'rsg-example': path.resolve(__dirname, 'packages')
  },
  sections: [
    {
      name: 'Hamed Design System',
      content: path.resolve('docs/intro/welcome.md')
    },
    {
      name: 'Components',
      description: 'The description for the installation section',
      sections: [
        {
          name: 'Layout',
          sections: [
            {
              name: 'Grid',
              components() {
                return [
                  path.resolve('packages/Grid/src/Grid.jsx'),
                  path.resolve('packages/Grid/src/Row/Row.jsx'),
                  path.resolve('packages/Grid/src/Col/Col.jsx')
                ]
              }
            }
          ]
        },
        {
          name: 'Links',
          components() {
            return [path.resolve('packages/Button/src/Button.jsx')]
          }
        }
      ]
    }
  ],
  version,
  theme: {
    fontFamily: {
      base: ['Montserrat', 'Open Sans', 'sans-serif', 'Arial']
    },
    color: {
      link: '#54595F',
      linkHover: '#d67a38',
      sidebarBackground: '#e4e4e4',
      codeBackground: '#e4e4e4'
    },
    sidebarWidth: 240
  },
  styles: {
    // Fixing mobile overflow of code examples
    Markdown: {
      pre: {
        'overflow-x': 'auto'
      }
    },
    ReactComponent: {
      tabs: {
        'overflow-x': 'auto'
      }
    },
    // [TDS-381] Increase font size in props tables to match default Paragraph size.
    Table: {
      cell: {
        fontSize: '0.8rem'
      },
      cellHeading: {
        fontSize: '1rem'
      }
    },
    Name: {
      name: {
        fontSize: 'inherit'
      }
    },
    Type: {
      type: {
        fontSize: 'inherit'
      }
    },
    Text: {
      text: {
        fontSize: 'inherit'
      }
    }
  },

  // template: styleguidistEnv === 'production' ? productionTemplate : devTemplate,
  webpackConfig: {
    devServer: {
      disableHostCheck: true,
      clientLogLevel: 'debug'
    },
    module: {
      rules: [
        {
          test: /\.(js|ts)x?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        }
      ],
      noParse: /\.(css|scss)/
    },
    resolve: {
      extensions: ['.js', 'jsx', '.json']
    }
  }
}
